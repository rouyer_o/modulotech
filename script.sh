#!/bin/bash
apt-get install pip
pip install -r requirement.txt
python manage.py makemigrations modulotech
python manage.py sqlmigrate modulotech 0001
python manage.py migrate
python manage.py runserver
python -mwebbrowser http://127.0.0.1:8000
