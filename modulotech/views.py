from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from .form import ContactForm, ConnexionForm, ProfileInfo
from django.http import *
from .models import *
from .settings import *
import os.path
"""
    Fonction de connexion / login
    """


@csrf_exempt
def login_user(request):
    try:
        if request.method == 'POST':
            form = ConnexionForm(data=request.POST)
            if form.is_valid():
                form.clean()
                user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
                if user is not None:
                    login(request, user)
                    return HttpResponseRedirect('/UserInfo/%s/' % user.id)
        else:
            form = ConnexionForm()
    except Exception:
        form = ConnexionForm()
        print("Error login_user")
    return render(request, 'registration/login.html', locals())

"""
Creer une annonce
"""
@csrf_exempt
def create_annonce(request):
    try:
        user = User.objects.get(id=request.user.id)
        if request.method == 'POST':
            print request.POST['image']
            annonce = Annonce.objects.create(title=request.POST['title'],image='annonce/'+request.POST['image'].replace(' ', '_'),description=request.POST['description'],price=request.POST['price'],address=request.POST['address'],ville=request.POST['ville'],code_postal=request.POST['code_postal'], user=user)
            annonce.save()
            if annonce is not None:
                annonce = 'Annonce cree'
    except user:
        annonce = 'Vous devez etre connecter'
    return render(request, 'front/create_annonce.html', locals())

#def save_image(name):
#    save_path = MEDIA_ROOT


"""
    Register Utilisateur
    """

def register_user(request):
    form = ContactForm(data=request.POST)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = ContactForm()
    return render(request, 'front/form.html', locals())


"""
Page d'accueil

"""

def index(request):
    try:
        annonce = Annonce.objects.all()
        if annonce is not None:
            print annonce[0].image
            #annonce['image'].replace('%','_')
            return render(request, 'front/index_annonce.html', locals())
        else:
            return render(request, 'front/index_annonce.html', {'annonce': 'Aucune annonce en database'})
    except Annonce.DoesNotExist:
        return render(request, 'front/index_annonce.html', { 'annnonce': 'aucune annonce existante'})


"""
    Sortir les infos users
    """
@csrf_exempt
def user_info(request, id_user):
    try:
        user = User.objects.get(id=id_user)
        print(user.username)
        annonce = Annonce.objects.filter(user=user.id)
        #info_user = InfoUser.objects.get(id=id_user)
        if not annonce:
            return render(request, "front/UserInfo.html",{ 'annonce': 'Aucune annonce !'})
        return render(request, "front/UserInfo.html", locals())
    except User.DoesNotExist:
        return redirect("/")


"""
Sortir les annonces selon l'utilisateur
"""

def display_user_annonce(request, id_user):
    try:
        annonce = Annonce.objects.get(user=id_user)
    except Annonce.DoesNotExist:
        return redirect("/")

"""
    Informations de contacts
    """
@csrf_exempt
def change_mail(request):
    p = User.objects.get(id=request.user.id)
    p.email = request.POST.get('email')
    p.save()
    return JsonResponse({'email': p.email})


"""
Page d'erreurs
"""

def error_404(request):
    data = {}
    return redirect(request, 'error_pages/error404.html', data)


def error_500(request):
    data = {}
    return redirect(request, 'error_pages/error500.html', data)


"""
Logout
"""

class LoggedOutView(TemplateView):
    template_name = "registration/logout.html"
