from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from settings import MEDIA_ROOT


class InfoUser(models.Model):
    user = models.ManyToManyField(User, blank=True, related_name='userModel')
    date_birth = models.DateField(auto_now_add=True, null=True)
    phone = models.CharField(max_length=12, blank=False, null=False)
    phone2 = models.CharField(max_length=12, blank=True, null=True)
    address = models.CharField(max_length=40, blank=True, null=True)
    ville = models.CharField(max_length=40, blank=True, null=True)
    code_postal = models.CharField(max_length=8, blank=True, null=True)
    site_internet = models.CharField(max_length=40, blank=True, null=True)
    avatar = models.ImageField(blank=True, null=True)
    facebook = models.URLField(max_length=40, blank=True, null=True)
    linkledink = models.URLField(max_length=40, blank=True, null=True)
    twitter = models.URLField(max_length=40, blank=True, null=True)
    social = models.URLField(max_length=40, blank=True, null=True)

    def __str__(self):
        return self.user.username


class Annonce(models.Model):
    db_table = 'Annonce'
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=500, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to='annonce')
    price = models.DecimalField(max_digits=5, decimal_places=2)
    address = models.CharField(max_length=40, blank=True, null=True)
    ville = models.CharField(max_length=40, blank=True, null=True)
    code_postal = models.CharField(max_length=8, blank=True, null=True)
    user =  models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
