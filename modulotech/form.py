from django.contrib.auth import get_user_model, authenticate
from django import forms
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.shortcuts import render
from .models import User, InfoUser

UserModel = get_user_model()


class ProfileInfo(forms.ModelForm):
    class Meta:
        model = InfoUser
        fields = '__all__'


class ContactForm(forms.Form):
    form_class = ProfileInfo
    template_name = 'front/form.html'

    model = UserModel
    fields = 'username', 'first_name', 'last_name', 'email', 'password'

    username = forms.CharField(label=('Username'), max_length=50, required=False,
                               widget=forms.TextInput(attrs={'autofocus': True}))
    first_name = forms.CharField(label=('First Name'), max_length=50, required=False, help_text=('Optional.'),
                                 widget=forms.TextInput(attrs={'autofocus': True}))
    last_name = forms.CharField(label=('Last Name'), max_length=50, required=False, help_text=('Optional.'))
    email = forms.EmailField(label=('Email'), max_length=255, help_text=('Required. Type a valid email address.'),
                             widget=forms.EmailInput(attrs={'placeholder': '@'}))

    password = forms.CharField(widget=forms.PasswordInput, help_text=None)
    confirm_password = forms.CharField(widget=forms.PasswordInput, help_text=None)
    error_messages = {
        'invalid_login': ('Please enter a correct email and password. Note that both fields may be case-sensitive.'),
        'inactive': ('This account is inactive.'),
    }

    def save(self, commit=True):
        if self.cleaned_data.get('confirm_password') == self.cleaned_data.get('password'):
            self.cleaned_data.pop('confirm_password')
            created = User.objects.create_user(**self.cleaned_data)
            if created:
                return HttpResponseRedirect('/login/')
            else:
                messages.add_message(messages.ERROR, ('Impossible de creer l"user'))
                return render(super(ContactForm, self), "", locals)
        else:
            return render(super(ContactForm, self), {'error': 'Les mots de passe ne correspondent pas'})


class ConnexionForm(forms.Form):
    username = forms.CharField(label=('Username'), max_length=50, required=True,
                               widget=forms.TextInput(attrs={'autofocus': True}))
    password = forms.CharField(label=('Password'), max_length=50, required=True,
                               widget=forms.PasswordInput(attrs={'autofocus': True}))

    error_messages = {
        'not_valid': ('This username is not valid')
    }

    def clean(self):
        try:
            cleaned_data = super(ConnexionForm, self).clean()
            if cleaned_data['username'] and cleaned_data['password']:
                username = cleaned_data.get('username')
                UserModel.objects.filter(username=username, password=cleaned_data['password']).count()
                print(UserModel.objects.filter(username=username, password=cleaned_data['password']))
                authenticate(username=cleaned_data['username'], password=cleaned_data['password'])
        except Exception:
            print("error clean in login_form")

    class Meta:
        model = User
        fields = 'username', 'password'

        widgets = {
            'class': 'form-control',
            'username': forms.TextInput(
                attrs={
                    'class': 'my-class',
                    'placeholder': 'Name goes here',
                }),
            'password': forms.HiddenInput(
                attrs={
                    'class': 'my-class',
                    'placeholder': 'Password',
                }),
        }
