from django.contrib import admin
#from django.urls import path
from modulotech import views
from django.contrib.auth.views import logout
from django.conf.urls import url
from django.contrib.auth import views as auth_views




urlpatterns = [
    url('admin/', admin.site.urls),
    url('^register/$', views.register_user, name='register'),
    url('^new_annonce/$', views.create_annonce, name='create_annonce'),
    url('login/', views.login_user, name='login'),
    url('^$', views.index, name='index'),
    #url('changemail/', views.change_mail, name='change_mail'),
    url('logout/', auth_views.logout, {'template_name': 'registration/logout.html'}, name='logout'),
    url('^UserInfo/(?P<id_user>\d+)/', views.user_info, name='UserInfo'),
]
